# How to contribute ?

We strive to welcome all contributors! 

We want to be a project where contributing is both simple and inclusive, so please read the [Code of Conduct](CODE_OF_CONDUCT.md) (tldr: "We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.") first. You may also want to read the [license](LICENSE.md) (tldr: CC-BY-SA 4.0). 

Then, the preferred way to contribute is to submit a Merge Request on Gitlab. You can fork the project, edit in a branch of your fork, and present and submit your modifications.


## Brainstorming and discussion sessions

(Feb 3rd, 2021) Please note that we will discuss and work on the topics and precise contents of the MOOC in live brainstorming and discussions in February 2021, so if a particular topic is of interest to you, it's great to either attend, and/or leave your thoughts on the related issue, and only start to create content after these sessions when the actual contents of each wek will be agreed upon.



## Decision process

Ideally, the merging decisions will be taken based on [Consensus](https://en.wikipedia.org/wiki/Wikipedia:Consensus). The main difference with Wikipedia is that the modifications are approved before they are merged, while wiki editing is (mostly) post-publication consensus building, with the MR page as discussion space. 

In the case of major disagreements, ultimately, **for now**, the decision process ends with the <abbr title="Benevolent Dictator For Life">BDFL</abbr> process from the initiators of the project (i.e. Marc Jeanmougin), but once/if the community grows, more democratic processes could be discussed and adopted, if deemed beneficial for the project's health.




